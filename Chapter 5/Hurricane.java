/*
 * Hurricane.java
 * An application to display the strengh of a hurricane based upon category
 * Julian Webb
 * ICTP12
 * 07/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The Hurricane class displays the strengh of a hurricane based upon category
 */
public class Hurricane { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Object to collect input from user
        int category; //Category collected from user
        System.out.print("Enter Hurricane Category: "); //Ask User for hurricane category
        category = input.nextInt(); //Collect category
        switch (category) { //check different numbers compared to category
           case 0: System.out.print("No Hurricane"); break; //If 0 then No hurricane
           case 1: System.out.print("Category 1: 74-95 mph or 64-82 kt or 119-153 km/hr"); break; //If 1 then print Category 1 speeds
           case 2: System.out.print("Category 2: 96-110 mph or 83-95 kt or 154-177 km/hr"); break; //If 2 then print Category 2 speeds
           case 3: System.out.print("Category 3: 111-130 mph or 96-113 kt or 178-209 km/hr"); break; //If 3 then print Category 3 speeds
           case 4: System.out.print("Category 4: 131-155 mph or 114-135 kt or 210-249 km/hr"); break; //If 4 then print Category 4 speeds
           case 5: System.out.print("Category 5: greater than 155 mph or 135 kt or 249 km/hr"); break; //If 5 then print Category 5 speeds
           case 100: System.out.print("Category 100: Death"); break; //If 100 print death
        } //end switch
    } //end class definition
}