/*
 * Delivery.java
 * An application to display if a package can be shipped based upon the size
 * Julian Webb
 * ICTP12
 * 14/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The Delivery class displays if a package can be shipped based upon the size
 */
public class Delivery { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int length; //length of package from user
        int width; //width of package from user
        int height; //height of package from user
        System.out.print("Enter package dimensions:\n"); //Ask use for package size
        System.out.print("Length: "); //Ask for length
        length = input.nextInt(); //Collect length
        System.out.print("Width: "); //Ask for width
        width = input.nextInt(); //Collect width
        System.out.print("Height: "); //Ask for height
        height = input.nextInt(); //Collect height
        if ((length > 10) || (width > 10) || (height > 10)) { //If any dimension is too big
            System.out.print("Reject"); //Reject package
        } else { //If okay size
            System.out.print("Accept"); //Accept package
        } //endif
    } //end class definition
}