/*
 * Julian Webb
 * 28/10/11
 * Takes in data from the user then calculates and displays the distance of the race
 */
import java.util.*; //import all java utility packages

public class DistancePart2 {

    public static void main(String[] args) {
        double first;  //declare first  as a double
        double second; //declare second as a double
        double third;  //declare third  as a double
        double length; //declare length as a double
        Scanner input = new Scanner(System.in); //Create new abstract data type from object Scanner
        
        System.out.print("Enter the first segment length: "); //Print "Enter the first segment length: "
        first = input.nextDouble(); //set first to a Double from the user
        System.out.print("\nEnter the second segment length: "); //Print "Enter the second segment length: " on a newline
        second = input.nextInt();  //set second to a Double from the user
        System.out.print("\nEnter the third segment length: "); //Print "Enter the third segment length: " on a newline
        third = input.nextInt(); //set third to a Double from the user
        length = first + second + third; // and set it to  the addition first + second + third
        input.close(); //end imput check
        System.out.println("\nLength of Race: " + length + "km"); //Print 'Length of race: ' and length 
    } //End function main
} //End class DistancePart2