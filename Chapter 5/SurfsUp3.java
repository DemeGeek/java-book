/*
 * SurfsUp3.java
 * An application to get and display text depending on wave height
 * Julian Webb
 * ICTP12
 * 07/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The SurfsUp3 class gets and displays text depending on wave height
 */
public class SurfsUp3 { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int wave; //height of wave
        System.out.print("How high are the waves (feet)? "); //Ask for wave height
        wave = input.nextInt(); //Collect wave height
        if (wave >= 6) { //If wave is greater than or equal to 6
            System.out.print("Great Day for surfing!"); //Print 'Great Day for Surfing!'
        } else if (wave >= 3) { //If wave is greater than or equal to 3 (and less than 6)
            System.out.print("Go body boarding!"); //Print 'Go body boarding!'
        } else if (wave >= 0) { //If wave is greater than or equal to 0 (and less than 3)
            System.out.print("Go for a swim."); //Print 'Go for a swim'
        } else { //If anything else
            System.out.print("Wooah! What kinda surf is that?"); //Print 'Wooah! What kinda of surf is that?'
        } //endif
    } //end class definition
}