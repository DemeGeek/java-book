/*
 * Decay.java
 * An application to display a calculation to find intial amount, final amount, or half-life
 * Julian Webb
 * ICTP12
 * 09/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.lang.Math; //use package that lets use have better math stuffs
/*
 * The Decay class displays a calculation to find intial amount, final amount, or half-life
 */
public class Decay { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Object to collect input from user
        System.out.print("1. Final Amount\n2. Inital Amount\n3. Half-life (constant)\nFind: "); //Ask which equation to do
        double n, y, k, t; //Initalize intial mass, final mass, half-life and time in that order
        switch (input.nextInt()) { //Switch the input from the user
            case 1: //If user entered 1 (final mass)
                    System.out.print("Enter the inital mass: "); //Ask user for inital mass
                    n = input.nextDouble(); //Collect inital mass from user
                    System.out.print("Enter the time (years): "); //Ask user for time (years)
                    t = input.nextDouble(); //Collect time (years) from user
                    System.out.print("Enter half-life (Constant): "); //Ask user for half-life
                    k = input.nextDouble(); //Collect half-life from user
                    y = n / Math.exp((-k*t)); //Calculate final mass
                    System.out.print("Final Amount: " + y); //Display final mass
            break;
            case 2: //if user entered 2 (inital mass) 
                    System.out.print("Enter the final mass: "); //Ask user for final mass
                    y = input.nextDouble(); //Collect final mass
                    System.out.print("Enter the time (years): "); //Ask user for time (years)
                    t = input.nextDouble(); //Collect time (years)
                    System.out.print("Enter half-life (Constant): "); //Ask user for half-life
                    k = input.nextDouble(); //Collect half-life from user
                    n = y / Math.exp(k*t); //Calculate inital mass
                    System.out.print("Inital Amount: " + n); //Display inital mass
            break;
            case 3:
                    System.out.print("Enter the inital mass: "); //Ask user for inital mass
                    n = input.nextDouble(); //Collect inital mass from user
                    System.out.print("Enter the final mass: "); //Ask user for final mass
                    y = input.nextDouble(); //Collect final mass
                    System.out.print("Enter the time (years): "); //Ask user for time (years)
                    t = input.nextDouble(); //Collect time (years)
                    k = Math.log(y / n) / t;
                    System.out.print("Constant Half-life: " + k); //Display half-life
            break;
            default: System.out.print("Really?"); break; //Easter egg if you enter a number not on the menu
        }
    } //end class definition
}