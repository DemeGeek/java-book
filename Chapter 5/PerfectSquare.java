/*
 * PerfectSquare.java
 * An application to display if a number is a perfect square
 * Julian Webb
 * ICTP12
 * 14/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.lang.Math; //use package that lets us square root easily
/*
 * The PerfectSquare class displays if a number is a perfect square
 */
public class PerfectSquare { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Number: "); //Ask user for number
        int usrIn = input.nextInt(); //Collect number in usrIn (int)
        if (usrIn == ((int)Math.sqrt(usrIn) * (int)Math.sqrt(usrIn))) { //check if is perfect square
            System.out.print("Perfect Square"); //display "Perfect Square"
        } else { //If not perfect
            System.out.print("Not Perfect Square"); //display "Not Perfect Square"
        } // end if
    } //end class definition
}