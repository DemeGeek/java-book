/*
 * Printing.java
 * An application to display the cost of printing depending on the number of pages to print
 * Julian Webb
 * ICTP12
 * 15/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.text.NumberFormat; //use package that lets us format numbers
/*
 * The Hurricane class displays the cost of printing depending on the number of pages to print
 */
public class Printing { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Object to collect input from user
        NumberFormat CAD = NumberFormat.getCurrencyInstance(); //Number Format
        int numCopies; //number of copies
        System.out.print("Enter the number of copies to print: "); //Ask User for number of copies
        numCopies = input.nextInt(); //Collect number of copies
        switch (numCopies) { //check number of copes
           case -1: System.out.print("No Cost"); break; //If null then No copies
           case 99: System.out.println("Price per copy: $0.30"); //If 0-99 then price per copy is 0.30
                    System.out.println("Total Cost is: " + CAD.format(numCopies * 0.30)); //Print total cost
                    break;
           case 499: System.out.println("Price per copy: $0.28"); //If 100-499 then price per copy is 0.28
                    System.out.println("Total Cost is: " + CAD.format(numCopies * 0.28)); //Print total cost
                    break;
           case 749: System.out.println("Price per copy: $0.27"); //If 500-749 then price per copy is 0.27
                    System.out.println("Total Cost is: " + CAD.format(numCopies * 0.27)); //Print total cost
                    break;
           case 1000: System.out.println("Price per copy: $0.26"); //If 750-1000 then price per copy is 0.26
                    System.out.println("Total Cost is: " + CAD.format(numCopies * 0.26)); //Print total cost
                    break;
           default: System.out.println("Price per copy: $0.25"); //If over 1000 then price per coppy is 0.25
                    System.out.println("Total Cost is: " + CAD.format(numCopies * 0.25)); //Print total cost
                    break;
        } //end switch
    } //end class definition
}