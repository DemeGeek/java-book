/*
 * NumberGenerator.java
 * An application to display pesudo-randomly generated numbers using the Linear Congruential Method
 * Julian Webb
 * ICTP12
 * 15/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
/*
 * The NumberGenerator class displays pesudo-randomly generated numbers using the Linear Congruential Method
 */
public class NumberGenerator { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Object to collect input from user
        final double a = 1246, c = 200, m = 50; //Number that seem to need to stay the same
        double s = 0.12, ans; //the seed and the answer to the equations
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m;
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
        s = ans; //Change the seed to the answer
        ans = (a*s + c) % m; //Do the equation
        System.out.println("("+a+"*"+s+" + "+c+") % "+m+"="+ans); //Display  equation and answer
    } //end class definition
}