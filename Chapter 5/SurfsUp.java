/*
 * SurfsUp.java
 * An application to get and display text depending on wave height
 * Julian Webb
 * ICTP12
 * 03/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The SurfsUp class gets and displays text depending on wave height
 */
public class SurfsUp { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int wave; //height of wave
        System.out.print("How high are the waves (feet)? "); //Ask for wave height
        wave = input.nextInt(); //Collect wave height
        if (wave >= 6) { //If wave is greater than or equal to 5
            System.out.print("Great Day for surfing!"); //Print 'Great Day for Surfing!'
        } //endif
    } //end class definition
}