/*
 * RandomNum.java
 * An application to display a random number based upon a min and max given by user
 * Julian Webb
 * ICTP12
 * 09/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.lang.Math; //use package that lets use have better math stuffs
/*
 * The RandomNum class displays a random number based upon a min and max given by user
 */
public class RandomNum { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Object to collect input from user
        int min; //Minimum for range
        int max; //Maximum for range
        System.out.print("Minimum: "); //Ask user for Minimum
        min = input.nextInt(); //Collect minimum
        System.out.print("Maximum: "); //Ask user for Maximum
        max = input.nextInt(); //Collect maximum
        System.out.print((int)((max - min + 1) * Math.random() + min)); //Display random in range
    } //end class definition
}