/*
 * Julian Webb
 * 28/10/11
 * Calculates and displays the distance of the race
 */
public class DistancePart1 {

    public static void main(String[] args) {
        double first = 12.2;  //declare first  as a double and set it to 12.2
        double second = 10.6; //declare second as a double and set it to 10.6
        double third = 5.8;   //declare third  as a double and set it to  5.8
        double length;        //declare length as a double
        length = first + second + third;                // and set it to ~28.6 (first + second + third)
        
        System.out.println("Length of Race: " + length + "km"); //Print 'Perimeter of rectangle: ' and perimeter 
    } //End function main
} //End class DistancePart1