/*
 * AboutMe2.java
 * An About Me Java Application
 * Julian Webb
 * ICTP12
 * 17/10/11
 */

/**
 * The AboutMe class displays the information
 */
public class AboutMe2 { //start class definition

    public static void main(String[] args) {
            System.out.println("Julian W"); //print Julian W
            System.out.println("Lee Rachar"); //newline, print Lee Rachar
            System.out.println("King George Secondary");//newline, print King George Secondary
            System.out.println("Go Dragons!");//newline, print Go Dragons!
            System.out.println("");
            /*
             * Day 1 Schedule
             */
            System.out.format("%-10s %8s %8s", "French 11", "Day 1", "8:29 - 9:45\n");
            System.out.format("%-10s %8s %8s", "ICTP12", "Day 1", "10:25 - 11:40\n");
            System.out.format("%-10s %8s %8s", "English 11", "Day 1", "12:30 - 1:45\n");
            System.out.format("%-10s %8s %8s", "ICTM11", "Day 1", "1:50 - 3:06\n");
            System.out.println("");
            /*
             * Day 2 Schedule
             */
            System.out.format("%-10s %8s %8s", "Pre-Calculus 11", "Day 2", "8:29 - 9:45\n");
            System.out.format("%-10s %8s %8s", "Graphic Design 11", "Day 2", "10:25 - 11:40\n");
            System.out.format("%-10s %8s %8s", "Physics 11", "Day 2", "12:30 - 1:45\n");
            System.out.format("%-10s %8s %8s", "Chemistry 11", "Day 2", "1:50 - 3:06");
        }
    } //end class definition