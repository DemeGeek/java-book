/*
 * AboutMe.java
 * An About Me Java Application
 * Julian Webb
 * ICTP12
 * 17/10/11
 */

/**
 * The AboutMe class displays the information
 */
public class AboutMe { //start class definition

    public static void main(String[] args) {
            System.out.println("Julian W"); //print Julian W
            System.out.println("Lee Rachar"); //newline, print Lee Rachar
            System.out.println("King George Secondary");//newline, print King George Secondary
            System.out.println("Go Dragons!");//newline, print Go Dragons!
        }
    } //end class definition