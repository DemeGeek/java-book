/*
 * Smile.java
 * A first Java application
 * Julian Webb
 * ICTP12
 * 17/10/11
 */

/*
 * The Smile class displays a Smiling Face.
 */
public class Smile { //start class definition
    public static void main(String[] args) {
            //Print the smiley face a row at a time in 9 rows
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", " ", " ", " ", "*", "*", "*", "*", "*", " ", " ", " ", " \n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", " ", " ", "*", " ", " ", " ", " ", " ", "*", " ", " ", " \n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", " ", "*", " ", "_", " ", " ", " ", "_", " ", "*", " ", " \n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", "*", " ", " ", "o", " ", " ", " ", "o", " ", " ", "*", " \n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "*", " ", " ", " ", " ", " ", "|", " ", " ", " ", " ", " ", "*\n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", "*", " ", " ", " ", " ", " ", "+", " ", " ", " ", " ", " ", "*\n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", "*", " ", " ", "\\", "_", "_", "_", "/", " ", " ", "*", " \n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", " ", "*", " ", " ", " ", " ", " ", " ", " ", "*", " ", " \n");
           System.out.format("%1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s %1s", " ", " ", " ", "*", "*", "*", "*", "*", "*", "*", " ", " ", " ");
        }
    } //end class definition