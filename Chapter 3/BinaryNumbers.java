/*
 * BinaryNumber.java
 * Julian Webb
 * ICTP12
 * 26/10/11
 */

/*
 * The BinaryNumber class displays a conversion chart between base-2, base-10 and base-16.
 */
public class BinaryNumbers { //start class definition

    public static void main(String[] args) {
            //write a conversion chart between Binary, Decimal and Hexadecimal up to Base-10 20.
            System.out.format("%8s %8s %8s", "Binary", "Decimal", "Hexadecimal\n");
            System.out.format("%8s %8s %8s", "1", "1", "1\n");
            System.out.format("%8s %8s %8s", "10", "2", "2\n");
            System.out.format("%8s %8s %8s", "11", "3", "3\n");
            System.out.format("%8s %8s %8s", "100", "4", "4\n");
            System.out.format("%8s %8s %8s", "101", "5", "5\n");
            System.out.format("%8s %8s %8s", "110", "6", "6\n");
            System.out.format("%8s %8s %8s", "111", "7", "7\n");
            System.out.format("%8s %8s %8s", "1000", "8", "8\n");
            System.out.format("%8s %8s %8s", "1001", "9", "9\n");
            System.out.format("%8s %8s %8s", "1010", "10", "A\n");
            System.out.format("%8s %8s %8s", "1011", "11", "B\n");
            System.out.format("%8s %8s %8s", "1100", "12", "C\n");
            System.out.format("%8s %8s %8s", "1101", "13", "D\n");
            System.out.format("%8s %8s %8s", "1110", "14", "E\n");
            System.out.format("%8s %8s %8s", "1111", "15", "F\n");
            System.out.format("%8s %8s %8s", "10000", "16", "10\n");
            System.out.format("%8s %8s %8s", "10001", "17", "11\n");
            System.out.format("%8s %8s %8s", "10010", "18", "12\n");
            System.out.format("%8s %8s %8s", "10011", "19", "13\n");
            System.out.format("%8s %8s %8s", "10100", "20", "14\n");
        }
    } //end class definition