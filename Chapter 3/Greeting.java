/*
 * Greeting.java
 * A first Java application
 * Julian Webb
 * ICTP12
 * 17/10/11
 */

/*
 * The Greeting class displays a greeting.
 */
public class Greeting { //start class definition

    public static void main(String[] args) {
            System.out.println("Hello, world!");
        }
    } //end class definition