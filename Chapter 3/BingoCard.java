/*
 * BingoCard.java
 * A Bingo Card Java Application
 * Julian Webb
 * ICTP12
 * 17/10/11
 */
/**
 * The BingoCard class displays the information
 */
public class BingoCard {
    public static void main(String[] args) {
            System.out.format("%8s %8s %8s %8s %8s", "B", "I", "N", "G", "O\n");//Print 5 columns with the data B I N G O
            System.out.format("%8s %8s %8s %8s %8s", "2", "20", "42", "60", "64\n");//Print 5 columns with the data 2 20 42 60 64
            System.out.format("%8s %8s %8s %8s %8s", "14", "25", "32", "55", "70\n");//Print 5 columns with the data 14 25 32 55 70
            System.out.format("%8s %8s %8s %8s %8s", "5", "18", "FREE", "53", "67\n");//Print 5 columns with the data 5 18 FREE 53 67
            System.out.format("%8s %8s %8s %8s %8s", "12", "16", "31", "46", "75\n");//Print 5 columns with the data 12 16 31 46 75
            System.out.format("%8s %8s %8s %8s %8s", "10", "22", "39", "59", "71\n");//Print 5 columns with the data 10 22 39 59 71
    }
}

