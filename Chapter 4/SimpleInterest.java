/*
 * SimpleInterest.java
 * An application to get and calculate how much money is gotten from interest
 * Julian Webb
 * ICTP12
 * 07/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.text.NumberFormat; //use package that lets us format numbers
/*
 * The SimpleInterest class gets and calculates how much money is gotten from interest
 */
public class SimpleInterest { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Create new Scanner object (Collects user's input)
        NumberFormat money = NumberFormat.getCurrencyInstance(); //Create new NumberFormat object (Formats numbers into currency format)
        double principal; //Variable to hold first amount
        double years;     //Variable to hold year  amount
        double interest;  //Variable to hold interest amount
        double amount;    //Variable to hold end   amount
        System.out.print("Enter the principal: "); //Ask user for first amount
        principal = input.nextDouble(); //Collect first amount
        System.out.print("Enter the number of years: "); //Ask user for year amount
        years = input.nextDouble(); //Collect year amount
        System.out.print("Enter the interest rate: "); //Ask user for interest amount
        interest = input.nextDouble(); //Collect interest amount
        amount = principal * (1 + years * interest); //Calculate end amount
        System.out.print("The value after the term is: " + money.format(amount)); //Print end amount formated as currency
        }
    } //end class definition