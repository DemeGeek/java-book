/*
 * Digits.java
 * An application to get the seperate digits from a two digit number
 * Julian Webb
 * ICTP12
 * 17/10/11
 */
import java.util.Scanner; //use package that lets us collect data from user
/*
 * The Digit class gets and displays the seperate digits from a two digit number
 */
public class Digit { //start class definition

    public static void main(String[] args) {
           int usrNum; //2 digit number that the user will give
           int digitOne; //Digit in the '1's column
           int digitTwo; //Digit in the '10's column
           Scanner input = new Scanner(System.in); //Object that collects user's input
           
           System.out.print("Enter a two-digit number: "); //Ask the user for a 2 digit number
           usrNum = input.nextInt(); //Collect int from user
           digitOne = usrNum % 10; //Get digit in ones place
           System.out.println("Digit in the ones place: " + digitOne); //display digit in ones place 
           digitTwo = (usrNum - digitOne) / 10; //get digit in tens place
           System.out.println("Digit in the tens place: " + digitTwo); //display digit in tens place
        }
    } //end class definition