/*
 * SimpleInterestB.java
 * An application to get and calculate how much money is needed to get end amount
 * Julian Webb
 * ICTP12
 * 07/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.text.NumberFormat; //use package that lets us format numbers
/*
 * The SimpleInterestB class gets and calculates how much money is needed to get end amountt
 */
public class SimpleInterestB { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Create new Scanner object (Collects user's input)
        NumberFormat money = NumberFormat.getCurrencyInstance(); //Create new NumberFormat object (Formats numbers into currency format)
        double principal; //Variable to hold first amount
        double years;     //Variable to hold year  amount
        double interest;  //Variable to hold interest amount
        double amount;    //Variable to hold end   amount
        System.out.print("Enter the amount: "); //Ask user for end amount
        amount = input.nextDouble(); //Collect end amount
        System.out.print("Enter the number of years: "); //Ask user for year amount
        years = input.nextDouble(); //Collect year amount
        System.out.print("Enter the interest rate: "); //Ask user for interest amount
        interest = input.nextDouble(); //Collect interest amount
        principal = amount / (1 + years * interest); //Calculate start amount
        System.out.print("The amount needed is: " + money.format(principal)); //Print end amount formated as currency
        }
    } //end class definition