/*
 * GradeAvg2.java
 * An application that averages 5 grades
 * Julian Webb
 * ICTP12
 * 03/11/11
 * 
 * I know that I am not suppose to know how to use while yet but the repetition was killing me
 * and it is like all the other while()s I have used before.
 */
import java.util.Scanner; //use package that lets us collect data from user
import java.text.NumberFormat; //use package that lest us change formating of numbers
/*
 * The GradeAvg2 class gets 5 grades and displays the average
 */
public class GradeAvg2 { //start class definition

    public static void main(String[] args) {
        double grade = 0; //Collected grades (set to 0)
        int num = 0; //Num of collected grades (set to 0)
        Scanner input = new Scanner(System.in); //Object that collects user's input
        NumberFormat percent = NumberFormat.getPercentInstance();
        
        System.out.println("Please enter 5 grades."); //Ask User for 5 grades
        while (num < 5) { //do this while var num is less than 5
            System.out.print("#"+(num+1)+": "); //Ask for grade
            grade += input.nextDouble(); //Add grade to others
            num++; //add 1 to var num
        } //end while
        System.out.println("Grade Average: " + percent.format(grade / 5)); //Display Average Grade
        } //end main
    } //end class definition