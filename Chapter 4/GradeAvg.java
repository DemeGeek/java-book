/*
 * GradeAvg.java
 * An application that averages 5 grades
 * Julian Webb
 * ICTP12
 * 01/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
/*
 * The GradeAvg class gets 5 grades and displays the average
 */
public class GradeAvg { //start class definition

    public static void main(String[] args) {
        double grdOne; //First collected grade
        double grdTwo; //Second collected grade
        double grdThree; //Third collected grade
        double grdFour; //Fourth collected grade
        double grdFive; //Fivith collected grade
        double grdAvg; //Average of collected grades
        Scanner input = new Scanner(System.in); //Object that collects user's input
        
        System.out.println("Please enter 5 grades."); //Ask User for 5 grades
        System.out.print("#1: "); //Ask for first
        grdOne = input.nextInt(); //Collect first
        System.out.print("#2: "); //Ask for second
        grdTwo = input.nextInt(); //Collect second
        System.out.print("#3: "); //Ask for third
        grdThree = input.nextInt(); //Collect third
        System.out.print("#4: "); //Ask for fourth
        grdFour = input.nextInt(); //Collect fourth
        System.out.print("#5: "); //Ask for fivith
        grdFive = input.nextInt(); //Collect fivith
        
        grdAvg = (grdOne + grdTwo + grdThree + grdFour + grdFive) / 5; //Get average of the 5 grades, uses real division is used as they are all doubles
        System.out.println("Grade Average: " + grdAvg); //Display Average Grade
        }
    } //end class definition