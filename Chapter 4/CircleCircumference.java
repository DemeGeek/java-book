/*
 * CircleCircumference.java
 * An application to find and display the circumference of a circle
 * Julian Webb
 * ICTP12
 * 03/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
/*
 * The Digit class finds and displays the circumference of a circle
 */
public class CircleCircumference { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final double PI = 3.1415926535; //set const PI to 3.1415926535
        double radius; //variable for radius of circle (data from User)
        double circum; //variable for circumference of circle (calculated)
        
        System.out.print("Enter a circle radius: "); //Asks user for radius
        radius = input.nextDouble(); //collects double from user for radius
        circum = 2 * PI * radius; //calculates circumference
        System.out.println("The Circumference for this circle is: " + circum); //displays circumference
        }
    } //end class definition