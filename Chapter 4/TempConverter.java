/*
 * TempConverter.java
 * An application to get and convert the temperature
 * Julian Webb
 * ICTP12
 * 03/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user
/*
 * The Digit class gets and converts the temperature
 */
public class TempConverter { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double intTemp; //Temperature that the User will give (fahrenheit)
        double endTemp; //Temperature that will be converted to (celsius)
        System.out.print("Enter a fahrenheit temperature: "); //Ask User for temperature (fahrenheit)
        intTemp = input.nextInt(); //Collect temperature from User (fahrenheit)
        endTemp = ((double)5 / (double)9) * (intTemp - (double)32); //Calculate temperature (celsius)
        System.out.println("Temperature in celsius: "+ endTemp); //Print temperature (celsius)
        }
    } //end class definition