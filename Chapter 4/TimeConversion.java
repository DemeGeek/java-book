/*
 * TimeConversion.java
 * An application to get and calculate the time
 * Julian Webb
 * ICTP12
 * 03/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The TimeConversion class gets and calculates the time
 */
public class TimeConversion { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double userIn; //variable to hold user input
        int endHr; //Number of hours calculated from userIn
        double endMin; //Number of minutes calculated from userIn
        
        System.out.print("Enter the time in minutes: "); //Ask User for temperature (fahrenheit)
        userIn = input.nextInt(); //Collect temperature from User (fahrenheit)
        endHr = (int)userIn / 60; //Calculate the number of hours from user input
        endMin = userIn % 60; //Calculate the number of minutes from user input
        System.out.println("The time is: "+ endHr + ":" + (int)endMin); //Display the time in proper format
        }
    } //end class definition