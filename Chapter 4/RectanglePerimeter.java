/*
 * Julian Webb
 * 28/10/11
 * Calculates and displays the perimeter of a rectange
 */
public class RectanglePerimeter {

    public static void main(String[] args) {
        int length = 13;                //declare length as an integer and give it a value of 13
        int width = 4;                  //declare width  as an integer and give it a value of  4
        int perimeter;               //declare perimeter as an integer
        perimeter = length * width;     //                             and give it a value of 52 (length * width)
        
        System.out.println("Perimeter of rectangle: " + perimeter); //Print 'Perimeter of rectangle: ' and perimeter 
    } //End function main
} //End class RectangePerimeter