/*
 *  Drawing Shapes and Changing Color Example
 */
import java.awt.*;
import java.applet.*;

public class Drawing extends Applet {
    
    public void paint(Graphics g) {
            setBackground(Color.black);
            g.setColor(Color.white);
            //Balloon
            g.drawOval(100, 100, 70, 50);
            g.drawOval(110, 100, 50, 50);
            g.drawOval(125, 100, 20, 50);
            g.drawArc(100, 115, 70, 20, 1, 1);
            //Box
            g.fillRect(125, 165, 20, 15);
            //Ropes
            g.drawLine(100, 130, 125, 165);
            g.drawLine(110, 130, 127, 165);
            g.drawLine(125, 130, 130, 165);
            g.drawLine(145, 130, 140, 165);
            g.drawLine(160, 130, 143, 165);
            g.drawLine(170, 130, 145, 165);
        }
    }