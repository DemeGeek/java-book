/*
 * Example.java
 */

import java.awt.*;
import java.applet.*;

public class Example extends Applet {
    
    String message;
    
    public void init() {
        message="My first Java applet";
    }

    public void paint(Graphics g) {
        g.setColor(Color.blue);
        g.drawString(message, 50, 60 );
    }
}