/*
 * NumbersSum.java
 * An application to get the factorial of a number
 * Julian Webb
 * ICTP12
 * 21/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The NumbersSum class gets the factorial of a number
 */
public class NumbersSum { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int sum = 0;
        System.out.print("Enter a number: ");
        int num = input.nextInt();
        for (int i = 0; i <= num; i++) {
            System.out.println(i);
            sum += i;
        }
        System.out.println("Sum is: "+sum);
    } //end class definition
}