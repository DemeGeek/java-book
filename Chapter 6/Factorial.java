/*
 * Factorial.java
 * An application to calculate the factorial of a number
 * Julian Webb
 * ICTP12
 * 23/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The Factorial class calculates the factorial of a number
 */
public class Factorial { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int usrIn; //input from user
        int factorial = 1; //factorial of usrIn
        System.out.println("The Super Handy Dandy Factorial Calculator (tm)"); //Name of program O:)
        System.out.print("Enter a number to find it's factorial: ");// Ask user for number
        usrIn = input.nextInt(); //Get number from user
        for (int count = 1; count <= usrIn; count++) //Until count (starting at 1) reaches usrIn by add one each time do this
        {
            factorial *= count; //multiply factorial by count
        } //end for
        System.out.println("Your factorial is: "+factorial); //Display the factorial
    } //end class definition
}