/*
 * PercentPassing.java
 * An application to calculate how may scores are passing
 * Julian Webb
 * ICTP12
 * 23/11/11
 */
import java.util.Scanner; //use package that lets us collect data from user

/*
 * The PercentPassing class calculates how may scores are passing
 */
public class PercentPassing { //start class definition

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final int SENTINEL = -1; //I choose -1 as flag becuase it is not possible to score a -1
        double usrIn = 0; //temp var for grades
        int pass = 0; //counter var for amount passing marks
        System.out.println("The super handy percent passing calculator!"); //intro sentence
        while (usrIn != SENTINEL) { //While the user has not tried to exit the loop, loop.
            System.out.print("Enter a grade or -1 to quit: "); //Ask user for grade or flag
            usrIn = input.nextDouble(); //Collect input
            if (usrIn > 70) { //If the number is higher than 70
                pass++; //Add one to the pass counter
            } //end if
        } //end while
        System.out.print("Number of passing grades is "+pass); //Display number of passing grades
    } //end class definition
}