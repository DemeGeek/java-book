/*
 * Evens.java
 * An application to display even numbers between 1 and 20
 * Julian Webb
 * ICTP12
 * 21/11/11
 */
/*
 * The Evens class displays even numbers between 1 and 20
 */
public class Evens { //start class definition

    public static void main(String[] args) {
        for (int i = 1; i <= 20; i++) { //Loop while the counter increments to 20 from 1
            if ((i % 2) == 0) { //If number is even
               System.out.println(i); //Print number
            } //end if
        }//end for
    } //end class definition
}