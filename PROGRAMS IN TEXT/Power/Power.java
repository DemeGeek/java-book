/*
 * Power.java from Chapter 13
 * An application for implementing a recursive power method.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * A recursive power method is implemented.
  */
 
  public class Power {
 	
 	/**
 	 * Returns num to the power power
 	 * pre: num and power are not 0.
 	 * post: num to the power power has been returned.
 	 */
 	public static int intPower(int num, int power) {
 		int result;
 		if (power == 0) {
 			result = 1;
 		} else {
 			result = num * intPower(num, power-1);
 		}
 		return(result);
 	}
 	

	public static void main(String[] args) {
		int x = intPower(2, 5);
		System.out.println(x);
	}
}