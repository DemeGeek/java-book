/*
 * LinkedListDemo.java from Chapter 14
 * A simple application for testing a linked list data structure.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * The linked list data structure is tested.
  */
 public class LinkedListDemo {

	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		
		list.addAtFront("Sachar");
		list.addAtFront("Osborne");
		list.addAtFront("Suess");
		System.out.println(list);
		
		list.remove("Suess");
		list.remove("Sachar");
		list.remove("Osborne");
		System.out.println(list);
	}
}