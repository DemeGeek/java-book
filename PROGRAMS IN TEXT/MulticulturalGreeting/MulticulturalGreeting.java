/*
 * MulticulturalGreeting.java from Chapter 3
 * An application that displays greetings in different languages.
 * Lawrenceville Press
 * June 10, 2005
 */

/**
 * The MulticulturalGreeting class display a welcome message.
 */
public class MulticulturalGreeting {

	public static void main(String[] args) {
		System.out.print("Hello\t");
		System.out.print("Buenos Dias\t");
		System.out.println("Bonjour");
		System.out.println("How do you greet another?");

	}
}
