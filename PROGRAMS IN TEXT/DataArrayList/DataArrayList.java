/*
 * DataArrayList.java from Chapter 10
 * ArrayList with Integer objects example.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * Demonstrates wrapper classes.
  */
 
 import java.util.ArrayList;
  
 public class DataArrayList {

	public static void main(String[] args) {
		ArrayList numbers = new ArrayList();
		Integer element, element1, element2;
		int sum = 0;
		
		numbers.add(new Integer(5));
		numbers.add(new Integer(2));

		/* compare values */
		element1 = (Integer)numbers.get(0);
		element2 = (Integer)numbers.get(1);
		if (element1.compareTo(element2) == 0) {
			System.out.println("The elements have the same value.");
		} else if (element1.compareTo(element2) < 0) {
			System.out.println("element1 value is less than element2.");
		} else {
			System.out.println("element1 value is greater than element2.");
		}
		
		/* sum values */
		for (Object num : numbers) {
			element = (Integer)num;
			sum += element.intValue();
		}
		
		System.out.println("Sum of the elements is: " + sum);	//6
	}
}