/*
 * AverageValue.java from Chapter 6
 * An application for demonstrating the use of counters and accumulators.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 import java.util.Scanner;
 
 /**
 * Displays the average of a set of numbers
 */
 public class AverageValue {

	public static void main(String[] args) {
		final int SENTINEL = 0;
		int newValue;
		int numValues = 0;
		int sumOfValues = 0;
		double avg;
		Scanner input = new Scanner(System.in);
		
		/* Get a set of numbers from user */
		System.out.println("Calculate Average Program");
		System.out.print("Enter a value (" + SENTINEL + " to quit): ");
		newValue = input.nextInt();
		int num = -1;
		while (newValue != SENTINEL) {
			numValues += 1;
			sumOfValues += newValue;
			System.out.print("Enter a value (" + SENTINEL + " to quit): ");
			newValue = input.nextInt();
		}
		input.close();
		
		/*Calculate average of numbers entered by user */
		avg = (double)sumOfValues / (double)numValues;
		System.out.println("Average is " + avg);
	}

}