/*
 * TestFiles.java from Chapter 12
 * A program that demonstrates file creation.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 import java.io.*;
 
 /**
  * A program for demonstrating file objects.
  */
 public class TestFiles {

	public static void main(String[] args) {
		File textFile = new File("c:\\supplies.txt");
		if (textFile.exists()) {
			System.out.println("File already exists.");
		} else {
			try {
				textFile.createNewFile();
				System.out.println("New file created.");
			} catch (IOException e) {
				System.out.println("File could not be created.");
    			System.err.println("IOException: " + e.getMessage());
    		}
    	}
	}
}