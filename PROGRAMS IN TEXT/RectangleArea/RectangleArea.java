/*
 * RectangleArea.java from Chapter 4
 * An application for calculating the area of a rectangle.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * Calculates and displays the area of a rectangle
  */
 public class RectangleArea {

	public static void main(String[] args) {
		int length = 10;	//longer side of rectangle
		int width = 2;		//shorter side of rectangle
		int area;			//calculated area of rectangle

		area = length * width;
		System.out.println("Area of rectangle: " + area);
	}
}
