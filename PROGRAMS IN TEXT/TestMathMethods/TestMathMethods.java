/*
 * TestMathMethods.java from Chapter 5
 * An application that demonstrates the Math class.
 * Lawrenceville Press
 * June 10, 2005
 */
 
import java.lang.Math;

 /**
  * Demonstrates three of the Math methods
  */
 public class TestMathMethods {

	public static void main(String[] args) {
		int posNum = 12, negNum = -12;
		int num1 = 2, num2 = 6;
		int square = 49;
		
		System.out.println("The absolute value of " + posNum + 
							" is " + Math.abs(posNum));
		System.out.println("The absolute value of " + negNum + 
							" is " + Math.abs(negNum));
		System.out.println(num1 + " raised to the " + num2 + 
							" power is " + Math.pow(num1, num2));
		System.out.println("The square root of " + square + 
							" is " + Math.sqrt(square));
	}
}