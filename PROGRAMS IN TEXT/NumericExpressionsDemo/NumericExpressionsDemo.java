/*
 * NumericExpressionsDemo.java from Chapter 4
 * An application for demonstrating the use of expressions, 
 * operators, and order of operations.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * Displays the results of various numeric expressions
  */
 public class NumericExpressionsDemo {

	public static void main(String[] args) {
		int num1 = 5;
		int num2 = 3;
		int result;
		double doubleNum1 = 5;
		double doubleNum2 = 3;
		double doubleResult;
		
		System.out.println("num1 = " + num1);
		System.out.println("num2 = " + num2);
		System.out.println("doubleNum1 = " + doubleNum1);
		System.out.println("doubleNum2 = " + doubleNum2);

		result = num1 + num2;
		System.out.println("num1 + num2: " + result);
		
		result = num1 - num2;
		System.out.println("num1 - num2: " + result);

		result = num1 * num2;
		System.out.println("num1 * num2: " + result);
		
		result = num1 / num2;
		System.out.println("num1 / num2: " + result);
		
		doubleResult = doubleNum1 / doubleNum2;
		System.out.println("doubleNum1 / doubleNum2: " + doubleResult);

		doubleResult = num1 / doubleNum2;
		System.out.println("num1 / doubleNum2: " + doubleResult);

		result = num1 % num2;
		System.out.println("num1 % num2: " + result);

		doubleResult = doubleNum1 % doubleNum2;
		System.out.println("doubleNum1 % doubleNum2: " + result);
	}
}