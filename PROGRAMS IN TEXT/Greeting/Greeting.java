/*
 * Greeting.java from Chapter 3
 * A first Java application.
 * Lawrenceville Press
 * June 10, 2005
 */

package firstApplication;

/**
 * The Greeting class displays a greeting.
 */
public class Greeting {		//start class definition

	public static void main(String[] args) {
		System.out.println("Hello, world!");
	}

}							//end class definition