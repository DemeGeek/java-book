/*
 * RectangleArea2.java from Chapter 4
 * An application for calculating the area of a rectangle
 * after getting user input.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 import java.util.Scanner;
 
 /**
  * Calculates and displays the area of a rectangle
  * based on the width and length entered by the user.
  */
 public class RectangleArea2 {

	public static void main(String[] args) {
		int length;		//longer side of rectangle
		int width;		//shorter side of rectangle
		int area;		//calculated area of rectangle
		Scanner input = new Scanner(System.in);

		System.out.print("Enter the length: ");
		length = input.nextInt();
		System.out.print("Enter the width: ");
		width = input.nextInt();
		input.close();
		
		area = length * width;
		System.out.println("Area of rectangle: " + area);
	}

}