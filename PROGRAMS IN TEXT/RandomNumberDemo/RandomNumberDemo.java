/*
 * RandomNumberDemo.java from Chapter 5
 * An application for demonstrating the Java random number generator.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 import java.util.Random;
 
 /**
 * Demonstrates the Random class.
 */
 public class RandomNumberDemo {

	public static void main(String[] args) {
		Random r = new Random(10);
				
		System.out.println("First number: " + r.nextInt(100));
		System.out.println("Second number: " + r.nextInt(100));
		System.out.println("Third number: " + r.nextInt(100));
		System.out.println("Fourth number: " + r.nextInt(100));
		System.out.println("Fifth number: " + r.nextInt(100));
		
	}
}