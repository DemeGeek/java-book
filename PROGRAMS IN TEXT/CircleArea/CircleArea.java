/*
 * CircleArea.java from Chapter 4
 * An application for calculating the area of a circle.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
 * Calculates and displays the area of a circle
 */
 public class CircleArea {

	public static void main(String[] args) {
		final double PI = 3.14;
		double radius = 5;		//radius of circle
		double area;

		area = PI * radius * radius;
		System.out.println("Area of circle: " + area);
	}

}
