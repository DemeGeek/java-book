/*
 * CubeCalculator.java from Chapter 7
 * An application that demonstrates returning a value from a method.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * Demonstrates method with return values.
  */
 public class CubeCalculator {
	
	/**
	 * Calculates the cube of a number.
	 * pre: none
	 * post: x cubed returned
	 */
	public static double cubeOf(double x) {
		double xCubed;
		
		xCubed = x * x * x;
		return(xCubed);		
	}


	public static void main(String[] args) {
		double num = 2.0;
		double cube;
		
		cube = cubeOf(num);
		System.out.println(cube);
	}
}