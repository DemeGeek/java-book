/*
 * Example_Applet.java from Chapter 2
 * Lawrenceville Press
 * June 10, 2005
 */
 
import java.awt.*;
import java.applet.*;

public class Example_Applet extends Applet {
	
	String message;	
	
	public void init() {
		message="My first Java applet";
	}

	public void paint(Graphics g) {
		g.setColor(Color.blue);
		g.drawString(message, 50, 60 );
	}
}
