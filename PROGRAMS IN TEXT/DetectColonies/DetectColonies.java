/*
 * DetectColonies.java from Chapter 13
 * An application for analyzing colonies of bacteria.
 * Lawrenceville Press
 * June 10, 2005
 */
 
  
 public class DetectColonies { 	
 	
	public static void main(String[] args) {
		
		Slide culture = new Slide("slide.dat");
		culture.displaySlide();
		culture.displayColonies();
	}
}