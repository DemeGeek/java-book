/*
 * RightTriangle.java from Chapter 7
 * An application for methods with parameters.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * Draws a right triangle.
  */
 public class RightTriangle {
	
	public static void drawBar(int length, String mark) {
		
		for (int i = 1; i <= length; i++) {
			System.out.print(mark);
		}
		System.out.println();
	}

	
	public static void drawBar(int length) {
		
		for (int i = 1; i <= length; i++) {
			System.out.print("@");
		}
		System.out.println();
	}


	public static void main(String[] args) {
		
		/* draw a right triangle with base size 6 */
		for (int i = 1; i <= 6; i++) {
			drawBar(i);
		}
	}
}