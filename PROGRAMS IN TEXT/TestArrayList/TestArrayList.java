/*
 * Demonstrates the ArrayList structure from Chapter 10
 * Lawrenceville Press
 * June 10, 2005
 */


 import java.util.ArrayList;
  
 public class TestArrayList {

	public static void main(String[] args) {
		ArrayList myStrings = new ArrayList();
		
		myStrings.add("Kermit");
		myStrings.add("Lucille");
		myStrings.add("Sammy");
		myStrings.add("Roxy");
		myStrings.add("Myah");
		
		myStrings.remove("Roxy");
		
		for (Object name : myStrings) {
			System.out.println(name);
		}
	}
}