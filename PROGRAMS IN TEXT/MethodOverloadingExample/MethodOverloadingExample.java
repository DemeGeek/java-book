/*
 * MethodOverloadingExample.java from Chapter 7
 * An application with multiple methods of the same name.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 /**
  * Demonstrates method overloading.
  */
 public class MethodOverloadingExample {
	
	public static void drawBar(int length) {
		
		for (int i = 1; i <= length; i++) {
			System.out.print("*");
		}
		System.out.println();
	}


	public static void drawBar(int length, String mark) {
		
		for (int i = 1; i <= length; i++) {
			System.out.print(mark);
		}
		System.out.println();
	}


	public static void main(String[] args) {
		
		drawBar(10);
		drawBar(5, "@");
	}
}