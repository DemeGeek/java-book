/*
 * MiddleThree.java from Chapter 6
 * An application for demonstrating the length() and substring() String methods.
 * Lawrenceville Press
 * June 10, 2005
 */
 
 import java.util.Scanner;
 
 /**
 * Displays the middle three characters of a string.
 */
 public class MiddleThree {

	public static void main(String[] args) {
		String phrase, threeLetters;
		int phraseLength;
		int mid;
		Scanner input = new Scanner(System.in);
		
		/* get string from user */
		System.out.print("Enter text (at least three characters): ");
		phrase = input.nextLine();
		input.close();
		
		/* determine middle of phrase */
		phraseLength = phrase.length();
		mid = phraseLength / 2;
				
		/* display middle three characters */
		threeLetters = phrase.substring(mid - 1, mid + 2);
		System.out.println("Middle three characters are: " + threeLetters);
	}

}